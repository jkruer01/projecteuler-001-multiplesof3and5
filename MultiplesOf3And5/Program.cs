using System;
using System.Linq;

namespace MultiplesOf3And5
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var sum = Enumerable.Range(0, 1000).Where(n => (n % 3 == 0) || (n % 5 == 0)).Sum();
            Console.WriteLine("The sum of all multiples of 3 or 5 below 1000 is " + sum);
            Console.ReadLine();
        }
    }
}
